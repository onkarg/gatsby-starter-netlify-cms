import React from "react";
import { graphql, Link } from "gatsby";

export const NewPost = ({ data }) => {
  const { markdownRemark: post } = data;
  console.log("data", data);

  return (
    <div>
      <h2>{post.frontmatter.title}</h2>
      <div>{post.frontmatter.description}</div>
    </div>
  );
};

export default NewPost;

export const pageQuery = graphql`
  query NewPostByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        description
        tags
      }
    }
  }
`;
