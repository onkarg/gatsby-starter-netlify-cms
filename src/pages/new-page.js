import React from "react";
import { Link, graphql } from "gatsby";
import NewPost from "../templates/new-post";

class NewPage extends React.Component {
  render() {
    const { data } = this.props;
    const { edges: posts } = data.allMarkdownRemark;

    return (
      <div>
        {posts.map(({ node: post }) => {
          return (
            <>
              <h1>{post.frontmatter.title}</h1>
              <div>{post.frontmatter.description}</div>
            </>
          );
        })}
      </div>
    );
  }
}

export default NewPage;

export const pageQuery = graphql`
  query NewPageQuery {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: { frontmatter: { templateKey: { eq: "new-post" } } }
    ) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            templateKey
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`;
